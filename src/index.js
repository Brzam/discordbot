// .env
const dotenv = require('dotenv');
dotenv.config();

// Requires
const { Client, Intents, Collection } = require('discord.js');
const fs = require('node:fs');


// Instance
const client = new Client({
    intents: [
        Intents.FLAGS.GUILDS,
        Intents.FLAGS.GUILD_MESSAGES,
        Intents.FLAGS.GUILD_MESSAGE_REACTIONS
        ]
    });

// Generic Event Handling
const eventFiles = fs.readdirSync(__dirname + '/events').filter(file => file.endsWith('.js'));
for (const file of eventFiles) {
    const event = require(__dirname + `/events/${file}`)
    if (event.once) {
        client.once(event.name, (...args) => event.execute(...args));
    } else {
        client.on(event.name, (...args) => event.execute(...args));
    }
}

// Pixel Peeps Event Handling
const eventFilesPixelPeeps = fs.readdirSync(__dirname + '/events/pixelPeeps').filter(file => file.endsWith('.js'));
for (const file of eventFilesPixelPeeps) {
    const event = require(__dirname + `/events/pixelPeeps/${file}`)
    if (event.once) {
        client.once(event.name, (...args) => event.execute(...args));
    } else {
        client.on(event.name, (...args) => event.execute(...args));
    }
}

// Command Handling
client.commands = new Collection();
const commandFiles = fs.readdirSync(__dirname + '/commands').filter(file => file.endsWith('.js'));
for (const file of commandFiles) {
    const command = require(__dirname + `/commands/${file}`);

    // Adds each .js file to the collection
    client.commands.set(command.data.name, command);
}





// Commands

client.on('interactionCreate', async interaction => {
	if (!interaction.isCommand()) return;

	const command = client.commands.get(interaction.commandName);

	if (!command) return;

	try {
		await command.execute(interaction);
	} catch (error) {
		console.error(error);
		await interaction.reply({
            content: 'There was an error while executing this command!',
            ephemeral: true
        });
	}
});

// Login
client.login(process.env.ALFRED_TOKEN);
