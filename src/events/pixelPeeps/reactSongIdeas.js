module.exports = {
    name: 'messageCreate',
    execute(message) {
        if (message.channelId !== '962089071944015902') return;
        message.react('🟢')
            .then(() => message.react('🟡'))
            .then(() => message.react('🔴'))
    },
};