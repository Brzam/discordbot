module.exports = {
    name: 'messageCreate',
    execute(message) {
        if (message.channelId !== '962373771065430066') return;
        message.react('🟢')
            .then(() => message.react('🔴'))
            .then(() => message.react('<:YouTube:962113365747052595>'))
            .then(() => message.react('<:Twitch:962113498031194302>'))
            .then(() => message.react('💰'))
    },
};